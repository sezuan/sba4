PROJECT = sba4
PROJECT_DESCRIPTION = New project
PROJECT_VERSION = 0.1.0

DEPS = jiffy cache_tab lager p1_nif

LOCAL_DEPS = inets crypto mnesia ssl inets

dep_cache_tab = git https://github.com/processone/cache_tab.git 1.0.12
dep_p1_nif = git https://github.com/processone/p1_utils.git 1.0.10

include erlang.mk
