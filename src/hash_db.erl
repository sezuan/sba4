%%%-------------------------------------------------------------------
%%% File    : hash_db
%%% Author  : Matthias Rieber <matthias@zu-con.org>
%%% Purpose : Store Hashes, Delete Hashes based on index
%%% Purpose : https://developers.google.com/safe-browsing/v4/urls-hashing
%%% Created : 19 Oct 2017 by Matthias Rieber <matthias@zu-con.org>
%%%-------------------------------------------------------------------

-module(hash_db).
-author('matthias@zu-con.org').

-record(hash_malware, {hash= <<>>, metadata= #{}}).
-record(hash_social_engineering, {hash= <<>>, metadata= #{}}).
-record(hash_unwanted_software, {hash= <<>>, metadata= #{}}).
-record(metadata, {section, config= #{}}).

-export([init/1]).
-export([store/2, delete_index/2, clear/1,
         delete_indexes/2,checksum/1,
         get_metadata/2, get_metadata/3,
         atom_to_uppercase_binary/1, db_name/1,
         set_metadata/3, walk_through_and_match/2]).

-spec db_name(atom()) -> atom().

db_name(Hash) ->
    list_to_atom("hash_" ++ atom_to_list(Hash)).


-spec init(atom()) -> ok.

init(DB) ->
    ok= mnesia:start(),

    R= case DB of
        malware -> record_info(fields, hash_malware);
        social_engineering -> record_info(fields, hash_social_engineering);
        unwanted_software -> record_info(fields, hash_unwanted_software)
    end,

    ok= case catch mnesia:table_info(db_name(DB), storage_type) of
        {'EXIT', _} ->
            case mnesia:create_table(
                db_name(DB),
                [{attributes, R},
                 {type, ordered_set},
                 {local_content, true},
                 {ram_copies, [node()]}]) of
                {atomic, ok} -> ok;
                {aborted, _}= Reason1 -> Reason1
            end;
        _ -> ok
    end,

    case catch mnesia:table_info(metadata, storage_type) of
        {'EXIT', _} ->
            case mnesia:create_table(
                metadata,
                [{attributes, record_info(fields, metadata)},
                 {local_content, true},
                 {ram_copies, [node()]}]) of
                {atomic, ok} -> ok;
                {aborted, _}= Reason3 -> Reason3
            end;
        _ -> ok
    end.


-spec store(atom(), [binary()]) -> ok | error.

store(DB, Hashes) ->
    F= fun() ->
        lists:foreach(fun(X) ->
            R= case DB of
                malware -> #hash_malware{hash= X};
                social_engineering -> #hash_social_engineering{hash= X};
                unwanted_software -> #hash_unwanted_software{hash= X}
            end,
            mnesia:write(R)
        end, Hashes)
    end,
    case mnesia:transaction(F) of
        {atomic, ok} -> ok;
        _ -> error
    end.


-spec delete_indexes(atom(), [integer()]) -> ok | error.

delete_indexes(DB, Indexes) ->
    Hashes= walk_through_and_match(DB, Indexes),
    F= fun() ->
        lists:foreach(fun(X) ->
            delete_index(DB, X)
        end, Hashes)
    end,
    case mnesia:transaction(F) of
        {atomic, ok} -> ok;
        _ -> error
    end.


-spec delete_index(atom(), binary()) -> ok | error.

delete_index(DB, Hash) ->
    F= fun() ->
        mnesia:delete({db_name(DB), Hash})
    end,
    case mnesia:transaction(F) of
        {atomic, ok} -> ok;
        _ -> error
    end.


-spec walk_through_and_match(atom(), [integer()]) -> [binary()] | error.

walk_through_and_match(DB, SearchList) ->
    Search= lists:foldl(fun(X,A) ->
            maps:put(X, true, A)
        end, #{}, SearchList),
    F= fun() ->
        walk_through_and_match(db_name(DB), mnesia:first(db_name(DB)), 0, Search, [])
    end,
    case mnesia:transaction(F) of
        {atomic, R} -> R;
        _ -> error
    end.


-spec walk_through_and_match(atom(), binary() | '$end_of_table', integer(), #{}, [binary()]) -> [binary()].

walk_through_and_match(_,'$end_of_table', _, _, Hits) -> Hits;
walk_through_and_match(Tab, Key, Pos, Search, Hits) ->
    NewHits= case maps:is_key(Pos, Search) of 
        true -> [Key|Hits];
        false -> Hits
    end,
    NewKey= mnesia:next(Tab, Key),
    walk_through_and_match(Tab, NewKey, Pos+1, Search, NewHits).


-spec clear(atom()) -> ok | error.

clear(DB) ->
    case mnesia:clear_table(db_name(DB)) of
        {atomic, ok} -> ok;
        _ -> error
    end.


-spec checksum(atom()) -> binary().

checksum(DB) ->
    crypto:hash(
        sha256,
        lists:foldl(
            fun(X,A) ->
                <<A/binary, X/binary>>
            end, <<>>,
        lists:sort(mnesia:dirty_all_keys(db_name(DB))))).


-spec get_metadata(any(),any(),any()) -> {ok, any()} | {error, section_not_set|var_not_set}.

get_metadata(Section, Var, Default) ->
    case get_metadata(Section, Var) of
        {ok, X} -> {ok, X};
        _ ->{ok, Default}
    end.


-spec get_metadata(any(),any()) -> {ok, any()} | {error, section_not_set|var_not_set}.

get_metadata(Section, Var) ->
    case mnesia:dirty_read({metadata, Section}) of
        [] -> {error, section_not_set};
        [#metadata{config= #{Var := X}}] -> {ok, X};
        _ -> {error, var_not_set}
    end.


-spec set_metadata(atom(),atom(), any()) -> ok.

set_metadata(Section, Var, Value) ->
    F= fun() -> 
        Old= case mnesia:read({metadata, Section}) of
            [] -> #metadata{section= Section, config= #{}};
            [X] -> X
        end,
        mnesia:write(Old#metadata{config= (Old#metadata.config)#{Var => Value}})
    end,
    case mnesia:transaction(F) of
        {atomic, ok} -> ok
    end.


-spec atom_to_uppercase_binary(atom()) -> binary().

atom_to_uppercase_binary(Atom) ->
    list_to_binary(string:to_upper(atom_to_list(Atom))).
