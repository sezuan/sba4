%%%-------------------------------------------------------------------
%%% File    : req.erl
%%% Author  : Matthias Rieber <matthias@zu-con.org>
%%% Purpose : Sends API requests
%%% Purpose : https://developers.google.com/safe-browsing/v4/urls-hashing
%%% Created : 21 Oct 2017 by Matthias Rieber <matthias@zu-con.org>
%%%-------------------------------------------------------------------

-module(req).
-author('matthias@zu-con.org').

-export([build_tlu/1, tlu/1, update_db/1, build_fhf/2, fhf/2]).

-type databases() :: malware | social_engineering | unwanted_software.

-define(Url, "https://safebrowsing.googleapis.com/v4/").

%%%-------------------------------------------------------------------
%%%                           High Level API
%%%-------------------------------------------------------------------

-spec update_db([databases()]) -> {ok, list()} | {error, list()}.

update_db(DBs) ->
    TLUs= lists:foldl(fun(DB,A) -> 
        case hash_db:get_metadata(DB, state) of
            {ok, State} ->
                [#{threat_type => hash_db:atom_to_uppercase_binary(DB), state => State}|A];
            {error, _Error} ->
                [#{threat_type => hash_db:atom_to_uppercase_binary(DB)}|A]
        end end, [], DBs),
    R= tlu(TLUs),
    res_parse:threat_list_updates(R).


%%%-------------------------------------------------------------------
%%%                           API
%%%-------------------------------------------------------------------

-spec tlu([#{threat_type => binary(), any() => any()}]|#{threat_type => binary(), any() => any()}) -> map() | error.

tlu(S) when is_map(S) -> tlu([S]);
tlu(S) when is_list(S) ->
    Payload= jiffy:encode(build_tlu(S)),
    case httpc:request(post,
                  {?Url ++ "threatListUpdates:fetch?key=" ++ get_config(api_key),
                   [], "application/json", Payload},
                  [],[{body_format, binary}]) of
        {ok, {{_,200,_},_,Body}} ->
            jiffy:decode(Body, [return_maps]);
        _ -> error
    end.


-spec fhf(databases(), #{threat_type => binary(), any() => any()}) -> map() | error.

fhf(DB, S) ->
    Payload= jiffy:encode(build_fhf(DB, S)),
    case httpc:request(post,
                  {?Url ++ "fullHashes:find?key=" ++ get_config(api_key),
                   [], "application/json", Payload},
                  [],[{body_format, binary}]) of
        {ok, {{_,200,_},_,Body}} ->
            jiffy:decode(Body, [return_maps]);
        X -> X
    end.


%%%-------------------------------------------------------------------
%%%                       REQUESTS BUILDER
%%%-------------------------------------------------------------------

-spec build_tlu([#{threat_type => binary(), any() => any() }]) -> map().

build_tlu(S) ->
 T= lists:foldl(
    fun(X,A) ->
      R1= #{
        <<"threatType">> => maps:get(threat_type, X),
        <<"platformType">> => <<"ANY_PLATFORM">>,
        <<"threatEntryType">> => <<"URL">>,
        <<"constraints">> => #{
            <<"supportedCompressions">> => [<<"RAW">>]
        }
      },
      R2= case X of
        #{state := State} -> R1#{<<"state">> => State};
        _ -> R1
      end, [R2|A]
    end, [], S),
 #{
    <<"client">> => #{
      <<"clientId">> => <<"zu-con">>,
      <<"clientVersion">> => <<"0.0.0.0">>
    },
    <<"listUpdateRequests">> => T
 }.


-spec build_fhf(databases(), #{threat_type => binary(), any() => any() }) -> #{}.

build_fhf(DB, S) ->
 {ok, State} = hash_db:get_metadata(DB, state),
 #{
    <<"client">> => #{
      <<"clientId">> => <<"zu-con">>,
      <<"clientVersion">> => <<"0.0.0.0">>
    },
    <<"apiClient">> => #{
      <<"clientId">> => <<"zu-con">>,
      <<"clientVersion">> => <<"0.0.0.0">>
    },
    <<"clientStates">> => [ State ],
    <<"threatInfo">> => #{
        <<"threatTypes">> => [maps:get(threat_type, S)],
        <<"platformTypes">> => [<<"ANY_PLATFORM">>],
        <<"threatEntryTypes">> => [<<"URL">>],
        <<"threatEntries">> => [#{<<"hash">> => base64:encode(maps:get(hash, S))}]
    }
 }.

%%%-------------------------------------------------------------------
%%%                       MISC HELPER
%%%-------------------------------------------------------------------

-spec get_config(atom()) -> any().

get_config(P) ->
  case application:get_env(sba4, P) of
    {ok, V} -> V;
    _ -> undefined
  end.
