%%%-------------------------------------------------------------------
%%% File    : res_parse.erl
%%% Author  : Matthias Rieber <matthias@zu-con.org>
%%% Purpose : Parses API responses
%%% Purpose : https://developers.google.com/safe-browsing/v4/urls-hashing
%%% Created : 20 Oct 2017 by Matthias Rieber <matthias@zu-con.org>
%%%-------------------------------------------------------------------

-module(res_parse).
-author('matthias@zu-con.org').

-export([threat_list_updates/1]).

% res_parse:threat_list_updates(jiffy:decode(examples:fullupdate(), [return_maps])).

-spec lowercase_atom(binary()) -> atom().

lowercase_atom(Binary) ->
    list_to_atom(
        string:to_lower(binary_to_list(Binary))
    ).


-spec threat_list_updates(#{}) -> {ok,list()} | {error,list()}.

threat_list_updates(R) ->
    #{<<"listUpdateResponses">> := LUR}= R,
    UpdateGlobalStatus= lists:foldl(fun(LItem, {OldStatus, OldList}) ->
        #{<<"responseType">> := Type, <<"threatType">>:= DBB}= LItem,
        DB= lowercase_atom(DBB),
        case Type of
            <<"FULL_UPDATE">> -> hash_db:clear(DB);
            _ -> nothing
        end,
        case LItem of
            #{<<"removals">> := X1} -> parse_removals(DB, X1);
            _ -> nothing
        end,
        case LItem of
            #{<<"additions">> := X2} -> parse_additions(DB, X2);
            _ -> nothing
        end,

        #{<<"newClientState">> := NewClientState,
          <<"checksum">> := #{<<"sha256">> := ChecksumBase64}}= LItem,

        case base64:decode(ChecksumBase64) =:= hash_db:checksum(DB) of
            true -> 
                hash_db:set_metadata(DB,state,NewClientState),
                case OldStatus of
                    error -> {error, [{ok, DB} | OldList]};
                    ok -> {ok,  [{ok,DB} | OldList]}
                end;
            false ->
                hash_db:set_metadata(DB,state,<<"">>),
                case OldStatus of
                    error -> {error, [{error, DB} | OldList]};
                    ok -> {error, [{error, DB} | OldList]}
                end
        end
    end, {ok, []}, LUR),
    set_next_update_data(R), UpdateGlobalStatus.


-spec set_next_update_data(map()) -> ok.

set_next_update_data(#{<<"minimumWaitDuration">> := MWD}) ->
    hash_db:set_metadata(global, minimumWaitDuration, convert_mwd_to_seconds(MWD)),
    hash_db:set_metadata(global, lastUpdate, os:system_time(seconds));
set_next_update_data(#{}) -> ok.


-spec convert_mwd_to_seconds(binary()) -> integer().

convert_mwd_to_seconds(B) ->
   round(
     binary_to_float(
       re:replace(B, "^(.*)s$", "\\1",[{return, binary}])
     )
   ).


-spec parse_removals(atom(), [#{}]) -> ok | error.

parse_removals(DB, R) ->
    lists:foreach(fun(X) ->
        #{<<"rawIndices">> := #{<<"indices">> := Indexes}}= X,
        hash_db:delete_indexes(DB, Indexes)
    end, R).


-spec parse_additions(atom(), [#{}]) -> ok | error.

parse_additions(DB, R) ->
    lists:foreach(fun(X) ->
        #{<<"rawHashes">> := Hashes}= X,
        #{<<"prefixSize">> := PrefixSize, <<"rawHashes">> := RawHashes}= Hashes,
        hash_db:set_metadata(DB, hash_sizes,
            case hash_db:get_metadata(DB,hash_sizes) of
                {error, _} -> [PrefixSize];
                {ok, OldPrefixSize} -> lists:usort([PrefixSize|OldPrefixSize])
            end),
        O= [ Y || <<Y:PrefixSize/binary>> <= base64:decode(RawHashes) ],
        hash_db:store(DB, O)
    end, R).
