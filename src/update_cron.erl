-module(update_cron).
-behaviour(gen_server).

%% API.
-export([start_link/0]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

%% API.

-spec start_link() -> {ok, pid()}.
start_link() ->
	gen_server:start_link(?MODULE, [], []).


%% gen_server.

init([]) ->
    Timer = erlang:send_after(10000, self(), check),
	{ok, Timer}.

handle_call(_Request, _From, State) ->
	{reply, ignored, State}.

handle_cast(_Msg, State) ->
	{noreply, State}.

handle_info(check, OldTimer) ->
    _= erlang:cancel_timer(OldTimer),
    case sba4:updatedb() of
        {ok,_} -> ok;
        {error,_} -> ok
    end,
    Timer = erlang:send_after(60000, self(), check),
    {noreply, Timer};
handle_info(_Info, State) ->
	{noreply, State}.

terminate(_Reason, _State) ->
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.
