-module(sba4_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->
    ok= hash_db:init(malware),
    ok= hash_db:init(unwanted_software),
    ok= hash_db:init(social_engineering),
    ok= cache_tab:new(lookup, [{life_time, 600}, {max_size, 100000},{lru, false}]),
	sba4_sup:start_link().

stop(_State) ->
	ok.
