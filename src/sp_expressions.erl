%%%-------------------------------------------------------------------
%%% File    : sp_expressions
%%% Author  : Matthias Rieber <matthias@zu-con.org>
%%% Purpose : Create suffix/prefix expressions
%%% Purpose : https://developers.google.com/safe-browsing/v4/urls-hashing
%%% Created : 15 Oct 2017 by Matthias Rieber <matthias@zu-con.org>
%%%-------------------------------------------------------------------

-module(sp_expressions).
-author('matthias@zu-con.org').

-export([create/2, hashes/2]).


-spec create(binary(), integer()) -> [binary()].

create(Url, Length) ->
    {ok, {_Scheme, _User, Host, _Port, Path, Query, <<"">>}}= canonc:deconstruct_uri(Url),
    Ehost= host_expressions(Host),
    Epath= path_expressions(Path,Query),
    Expressions= [ << H/binary, P/binary >> || H <- Ehost, P <- Epath ],
    hashes(Expressions, Length).


-spec hashes([binary()], integer()) -> [binary()].

hashes(Urls, Length) ->
    HashUrls= [ crypto:hash(sha256, X) || X <- Urls ],
    lists:foldl(
        fun(X,A) ->
            <<Prefix:Length/binary, _/binary>>= X,
            [Prefix|A]
        end, [], HashUrls
    ).


-spec path_expressions(binary(), binary()) -> [binary()].

path_expressions(Path, Query)  ->
    Components= binary:split(Path, <<"/">>, [global]),
    SignificantComponents1= lists:sublist(Components, min(length(Components),5)),
    EndsWithDir= case lists:nth(1, lists:reverse(SignificantComponents1)) of
        <<>> -> true;
        _ -> case length(SignificantComponents1) of
            5 -> true;
            _ -> false
        end
    end,
    SignificantComponents= lists:sublist(Components, min(length(Components),4)),
        
    Part1= [<<Path/binary, Query/binary>>, <<Path/binary>>],

    Part2= lists:foldl(fun(X,A) ->
        case A of
            [] -> [<<"/">>];
            [L|_] ->
                case X of
                    <<>> -> A;
                    _ -> [<< L/binary, X/binary, <<"/">>/binary >> | A ]
                end
        end
    end, [], SignificantComponents),

    Part3= case EndsWithDir of
        true -> Part2;
        false ->
            [LastPath|Rest]= Part2,
            LastPathSize= size(LastPath)-1,
            case LastPathSize of
                LastPathSize when LastPathSize > 0 ->
                    <<FixedPath:LastPathSize/binary, _/binary>>= LastPath,
                    [FixedPath|Rest];
                _ -> Part2
            end
    end,
    lists:usort(Part1 ++ Part3).


-spec host_expressions(binary()) -> [binary()].

host_expressions(Host) ->
    case inet:parse_address(binary_to_list(Host)) of
    {ok, _} -> [Host];
    {error, _} ->
        AllComponents= binary:split(Host, <<".">>, [global]),
        SignificantComponents= case lists:reverse(AllComponents) of
            Components when length(Components) > 5 ->
                lists:sublist(Components, 5);
            Components -> Components
        end,
        Prefixes= lists:foldl(fun(X,A) ->
            case A of
                [] ->
                    [X];
                [L|_] ->
                    [<< X/binary, <<".">>/binary, L/binary >> | A]
            end
        end, [], SignificantComponents),
        lists:usort(
        [Host] ++
        case Prefixes of
            Prefixes when length(Prefixes) =< 5 -> lists:sublist(Prefixes, length(Prefixes)-1);
            Prefixes -> lists:sublist(Prefixes, 4)
        end)
    end.

