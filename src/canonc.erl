%%%-------------------------------------------------------------------
%%% File    : canoc.erl
%%% Author  : Matthias Rieber <matthias@zu-con.org>
%%% Purpose : Canonicalize URLs
%%% Purpose : https://developers.google.com/safe-browsing/v4/urls-hashing
%%% Created : 11 Oct 2017 by Matthias Rieber <matthias@zu-con.org>
%%%-------------------------------------------------------------------

-module(canonc).
-author('matthias@zu-con.org').


%% that's all we do
-export([canonicalize/1]).

%% might be useful
-export([encode_url/1, deconstruct_uri/1, decode_url/1]).


%% -------------------------------------------------------------------
%% exported functions
%% -------------------------------------------------------------------

-spec canonicalize(binary()) -> binary().

canonicalize(Url) ->
    Url1= url_prepare(Url),
    Url2= fix_scheme(Url1),
    Url3= case deconstruct_uri(Url2) of
        %{error, _} -> error;
        {ok, {Scheme, _UserInfo, Host, _Port, Path, Query, Fragment}} ->
            CnHost= cn_hostname(Host),
            CnPath= cn_path(Path),
            << Scheme/binary,
               <<"://">>/binary,
               CnHost/binary, CnPath/binary, Query/binary, Fragment/binary >>
    end,
    encode_url(Url3).



%% -------------------------------------------------------------------
%% exported helper functions
%% -------------------------------------------------------------------
 
-spec encode_url(binary()) -> binary().

encode_url(Url) ->
    encode_url(Url, <<"">>).


-spec decode_url(binary()) -> binary().

decode_url(String) -> recurse_hexdecode(String, <<"">>).


-spec deconstruct_uri(binary()) -> {ok, {binary(), binary(), binary(),
    integer(), binary(), binary(), binary()}} | {noscheme, binary()}.

deconstruct_uri(Uri) ->
    {Scheme, Uri1}= case binary:match(Uri, <<"http://">>) of
        {0, _} -> {<<"http">>, binary:part(Uri, 7, byte_size(Uri)-7)};
        _ -> case binary:match(Uri, <<"https://">>) of
            {0, _} -> {<<"https">>, binary:part(Uri, 8, byte_size(Uri)-8)};
            _ -> {noscheme, Uri}
        end
    end,
    {UserDomain, PathQuery}= case binary:split(Uri1, <<"/">>) of
      [UserDomain1, P] -> {UserDomain1, << <<"/">>/binary, P/binary>>};
      [UserDomain1] -> {UserDomain1, <<"/">>}
    end,

    {Path, Query}= case binary:split(PathQuery, <<"?">>) of
        [Path1, QueryString1] -> {Path1, << <<"?">>/binary,
                                  QueryString1/binary >>};
        [Path1] -> {Path1, <<>>}
    end,

    {User, DomainPort}= case binary:split(UserDomain, <<"@">>, [global]) of
        DomainSplit when length(DomainSplit) > 1 ->
            [Domain1|DomainR] = lists:reverse(DomainSplit),
            {lists:foldl(fun(X,A) -> <<X/binary, <<"@">>/binary, A/binary>> end,
                <<>>, DomainR), Domain1};
        [Domain1] -> {<<"">>, Domain1}
    end,

    {Domain, Port}= case binary:split(DomainPort, <<":">>) of
        [Domain2, Port1] -> {Domain2, binary_to_integer(Port1)};
        [Domain2] ->
            case Scheme of
                <<"http">> -> {Domain2, 80};
                <<"https">> -> {Domain2, 443}
            end
    end,
    {ok, {Scheme, User, Domain, Port, Path, Query, <<"">>}}.



%% -------------------------------------------------------------------
%% internal functions
%% -------------------------------------------------------------------

-spec encode_url(binary(), binary()) -> binary().

encode_url(<<"">>, NewUrl) -> NewUrl;
encode_url(Url, NewUrl) ->
    <<Char/integer, R/binary>>= Url,
    NewChar= case Char of
        Char when Char =< 32 orelse Char >= 127 -> integer_to_percent_hex(Char);
        Char when Char =:= 35 -> integer_to_percent_hex(Char);
        Char when Char =:= 37 -> integer_to_percent_hex(Char);
        Char -> <<Char>>
    end,
    encode_url(R, << NewUrl/binary, NewChar/binary >>).


%% -------------------------------------------------------------------
%% internal helper functions
%% -------------------------------------------------------------------

-spec url_prepare(binary()) -> binary().

url_prepare(Url) ->
    %% FIXME: Remove Trainling and leading spaces
    Url0= re:replace(Url, "^ *", "", [{return, binary}]),
    Url1= re:replace(Url0, " *$", "", [{return, binary}]),
    %% Remove White Spaces
    Url2= re:replace(
        Url1,
        "[\x09\x0d\x0a]", "",
        [{return, binary}, global]
    ),
    {match, [Url3]}= re:run(
        Url2,
        "^([^#]*)",
        [{capture, all_but_first, binary}]
    ),
    Url4= decode_url(Url3),
    Url4.


-spec integer_to_percent_hex(integer()) -> binary().

integer_to_percent_hex(Integer) ->
    Hex= httpd_util:integer_to_hexlist(Integer),
    Hex1= case length(Hex) of
        1 -> "0" ++ Hex;
        2 -> Hex
    end,
    list_to_binary(
      "%" ++ Hex1
    ).


-spec fix_scheme(binary()) -> binary().

fix_scheme(Url) ->
    case binary:match(Url, <<"http://">>) of
        {0,_} -> Url;
        _ -> case binary:match(Url, <<"https://">>) of
            {0,_} -> Url;
            _ -> append_scheme(Url)
        end
    end.

append_scheme(Url) -> <<<<"http://">>/binary, Url/binary>>.


-spec recurse_hexdecode(binary(), binary()) -> binary().

recurse_hexdecode(String, OldString) when is_binary(String),
                                          is_binary(OldString) ->
    case String =:= OldString of
        true -> String;
        false -> recurse_hexdecode(hex_decode(String), String)
    end.


-spec hex_decode(binary()) -> binary().

hex_decode(String) -> hex_decode(String, <<>>).
hex_decode(<<>>, NewString) -> NewString;
hex_decode(String, NewString) ->
    case binary:split(String, <<"%">>) of
        [Pre, Post] when size(Post) >= 2 ->
            <<HexString:2/binary, RestPost1/binary>>= Post,
            {Decode, RestPost}= case catch hex2bin(HexString) of
                {'EXIT', _} -> {<< <<"%">>/binary >>,
                                <<HexString/binary, RestPost1/binary>>};
                UnHex -> {UnHex,RestPost1}
            end,
            hex_decode(RestPost, << NewString/binary, Pre/binary,
                                    Decode/binary >>);
        [Pre, Post] -> hex_decode(<<>>, <<NewString/binary,
                                          Pre/binary, <<"%">>/binary,
                                          Post/binary>>);
        [Pre] -> hex_decode(<<>>, <<NewString/binary, Pre/binary>>)
    end.


-spec hex2bin(binary()) -> binary().

hex2bin(Binary) ->
    F= list_to_integer(binary_to_list(Binary), 16),
    <<F>>.

      

-spec cn_hostname(binary()) -> binary().

cn_hostname(Hostname) when is_binary(Hostname) ->
    %% Remove Trailing and Leading Dots
    {match, [CnHostname2]}= re:run(
        Hostname,
        "^\\.*([^.].*[^.])\\.*$",
        [{capture, all_but_first, binary}]
    ),

    %% Fold dots
    CnHostname3= re:replace(
        CnHostname2,
        "\\.+", ".",
        [{return, binary}, global]
    ),

    CnHostname4= case inet:parse_address(binary_to_list(CnHostname3)) of
        {error, _} -> CnHostname3;
        {ok, IP} -> list_to_binary(inet:ntoa(IP))
    end,
    CnHostname5= lame_lowercase(CnHostname4),
    CnHostname5.


-spec cn_path(binary()) -> binary().

cn_path(Path) ->
    P1= remove_double_dots(Path),
    P2= remove_single_dot(P1),
    P3= remove_consecutive_slashes(P2),
    P3.

-spec remove_double_dots(binary()) -> binary().

remove_double_dots(Path) ->
    remove_double_dots(Path, <<"">>).


-spec remove_double_dots(binary(), binary()) -> binary().

remove_double_dots(Path, Path) -> Path;
remove_double_dots(Path, _) ->
    remove_double_dots(
      re:replace(Path, "[^/]+/\\.\\.", "/", [{return, binary}]), Path).


-spec remove_single_dot(binary()) -> binary().

remove_single_dot(Path) ->
    remove_single_dot(Path, <<"">>).
remove_single_dot(Path, Path) -> Path;
remove_single_dot(Path, _) ->
    P1= re:replace(Path, "/\\./", "/", [{return, binary}]),
    remove_single_dot(re:replace(P1, "/\\.$", "/", [{return, binary}]), P1).


-spec remove_consecutive_slashes(binary()) -> binary().

remove_consecutive_slashes(Path) ->
    remove_consecutive_slashes(Path, <<"">>).


-spec remove_consecutive_slashes(binary(), binary()) -> binary().

remove_consecutive_slashes(Path, Path) -> Path;
remove_consecutive_slashes(Path, _) ->
    remove_consecutive_slashes(
      re:replace(Path, "//", "/", [{return, binary}]), Path).


-spec lame_lowercase(binary()) -> binary().

lame_lowercase(Binary) ->
    lame_lowercase(Binary, <<"">>).


-spec lame_lowercase(binary(), binary()) -> binary().

lame_lowercase(<<>>,R) -> R;
lame_lowercase(<<C/integer, R/binary>>, New) when C =< 90 andalso C >= 65 ->
    lame_lowercase(R, <<New/binary, <<(C+32)>>/binary>>);
lame_lowercase(<<C/integer, R/binary>>, New) ->
    lame_lowercase(R, <<New/binary, <<C>>/binary>>).
 
