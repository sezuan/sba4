%%%-------------------------------------------------------------------
%%% File    : sba4.erl
%%% Author  : Matthias Rieber <matthias@zu-con.org>
%%% Purpose : 
%%% Purpose : https://developers.google.com/safe-browsing/v4/urls-hashing
%%% Created : 22 Oct 2017 by Matthias Rieber <matthias@zu-con.org>
%%%-------------------------------------------------------------------

-module(sba4).
-author('matthias@zu-con.org').

-compile([{parse_transform, lager_transform}]).

-export([lookup/1, updatedb/0]).

-type databases() :: malware | social_engineering | unwanted_software.


%%%-------------------------------------------------------------------
%%%                           High Level API
%%%-------------------------------------------------------------------

-spec updatedb() -> {ok, list() | {not_yet, integer()}} | {error, list()}.

updatedb() ->
    {ok, LastUpdate}= hash_db:get_metadata(global, lastUpdate, 0),
    {ok, MinimumWaitDuration}= hash_db:get_metadata(global, minimumWaitDuration, 0),
    Now= os:system_time(seconds),
    case (LastUpdate + MinimumWaitDuration) =< Now of
        true ->
            Databases= [malware, social_engineering, unwanted_software],
            req:update_db(Databases);
        false -> {ok, {not_yet, (LastUpdate + MinimumWaitDuration)-Now}}
    end.


-spec lookup(binary()) -> ok | {error, atom()}.

lookup(Url) ->
    Databases= [malware, social_engineering, unwanted_software],
    {ok, F}= cache_tab:lookup(lookup, Url, fun() -> {ok, lookup(Databases, Url, ok)} end),
    F.


%%%-------------------------------------------------------------------
%%%                          Private Functions
%%%-------------------------------------------------------------------

-spec lookup(list(), binary(), (ok | {error, atom()})) -> ok | {error, atom()}.

lookup(_,_,{error, E}) -> {error, E};
lookup([],_,_) -> ok;
lookup([DB|Rest], Url, ok) ->
    case catch lookup(Rest, Url, lookup(DB, Url)) of
      {'EXIT',_} ->
        ok= lager:error("url check in DB ~p failed: ~p", [DB, Url]),
        ok;
      X ->
        ok= lager:info("url check for ~p in ~p with result: ~p", [Url, DB, X]),
        X
    end.


-spec lookup(atom(), binary()) -> ok | {error, databases()}.

lookup(DB, Url) ->
    Url1= canonc:canonicalize(Url),
    {ok, HashSizes}= hash_db:get_metadata(DB, hash_sizes),
    HashedUrls= lists:foldl(fun(X, A) ->
       sp_expressions:create(Url1, X) ++ A
    end, [], HashSizes),
    case search_hash(DB, HashedUrls) of
        ok -> ok;
        {found, Lookup} ->
            case req:fhf(DB, #{threat_type => hash_db:atom_to_uppercase_binary(DB), hash => Lookup}) of
                #{<<"matches">> := [#{<<"threat">> := #{<<"hash">> := FullHash}}]} ->
                   case compare_with_fullhashes(Url, FullHash) of
                     true -> {error, DB};
                     false -> ok
                   end;
                _ -> ok
            end
    end.


-spec search_hash(databases(), [binary()]) -> ok | {found, binary()}.

search_hash(DB, Hashes) ->
    search_hash(DB, Hashes, notfound).


-spec search_hash(databases(), [binary()] | [], notfound | {found,binary()}) -> ok | {found, binary()}.

search_hash(_DB, [], notfound) -> ok;
search_hash(_DB, _, {found, _Lookup}= R) -> R;
search_hash(DB, [Lookup|Rest], notfound) ->
    case mnesia:dirty_read({hash_db:db_name(DB), Lookup}) of
        [] -> search_hash(DB, Rest, notfound);
        _ -> search_hash(DB, Rest, {found, Lookup})
    end.

-spec compare_with_fullhashes(binary(), binary()) -> true | false.

compare_with_fullhashes(Url, FullHash) ->
    FullHashes= lists:foldl(
        fun(X,A) ->
            [base64:encode(X)|A]
        end, [],
        sp_expressions:create(
            canonc:canonicalize(Url),32)
    ),
    lists:member(FullHash, FullHashes).
