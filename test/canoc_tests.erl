-module(canoc_tests).
-include_lib("eunit/include/eunit.hrl").

canonicalize_test_() -> [canonicalize()].

canonicalize() ->
    [
        ?_assertEqual(<<"http://host/%25">>, canonc:canonicalize(<<"http://host/%25%32%35">>)),
        ?_assertEqual(<<"http://host/%25%25">>, canonc:canonicalize(<<"http://host/%25%32%35%25%32%35">>)),
        ?_assertEqual(<<"http://host/%25">>, canonc:canonicalize(<<"http://host/%2525252525252525">>)),
        ?_assertEqual(<<"http://host/asdf%25asd">>, canonc:canonicalize(<<"http://host/asdf%25%32%35asd">>)),
        ?_assertEqual(<<"http://host/%25%25%25asd%25%25">>, canonc:canonicalize(<<"http://host/%%%25%32%35asd%%">>)),
        ?_assertEqual(<<"http://www.google.com/">>, canonc:canonicalize(<<"http://www.google.com/">>)),
        ?_assertEqual(<<"http://168.188.99.26/.secure/www.ebay.com/">>, canonc:canonicalize(<<"http://%31%36%38%2e%31%38%38%2e%39%39%2e%32%36/%2E%73%65%63%75%72%65/%77%77%77%2E%65%62%61%79%2E%63%6F%6D/">>)),
        ?_assertEqual(<<"http://195.127.0.11/uploads/%20%20%20%20/.verify/.eBaysecure=updateuserdataxplimnbqmn-xplmvalidateinfoswqpcmlx=hgplmcx/">>, canonc:canonicalize(<<"http://195.127.0.11/uploads/%20%20%20%20/.verify/.eBaysecure=updateuserdataxplimnbqmn-xplmvalidateinfoswqpcmlx=hgplmcx/">>)),
        ?_assertEqual(<<"http://host%23.com/~a!b@c%23d$e%25f^00&11*22(33)44_55+">>, canonc:canonicalize(<<"http://host%23.com/%257Ea%2521b%2540c%2523d%2524e%25f%255E00%252611%252A22%252833%252944_55%252B">>)),
        ?_assertEqual(<<"http://195.127.0.11/blah">>, canonc:canonicalize(<<"http://3279880203/blah">>)),
        ?_assertEqual(<<"http://www.google.com/">>, canonc:canonicalize(<<"http://www.google.com/blah/..">>)),
        ?_assertEqual(<<"http://www.google.com/">>, canonc:canonicalize(<<"www.google.com/">>)),
        ?_assertEqual(<<"http://www.google.com/">>, canonc:canonicalize(<<"www.google.com">>)),
        ?_assertEqual(<<"http://www.evil.com/blah">>, canonc:canonicalize(<<"http://www.evil.com/blah#frag">>)),
        ?_assertEqual(<<"http://www.google.com/">>, canonc:canonicalize(<<"http://www.GOOgle.com/">>)),
        ?_assertEqual(<<"http://www.google.com/">>, canonc:canonicalize(<<"http://www.google.com.../">>)),
        ?_assertEqual(<<"http://www.google.com/foobarbaz2">>, canonc:canonicalize(<<"http://www.google.com/foo\tbar\rbaz\n2">>)),
        ?_assertEqual(<<"http://www.google.com/q?">>, canonc:canonicalize(<<"http://www.google.com/q?">>)),
        ?_assertEqual(<<"http://www.google.com/q?r?">>, canonc:canonicalize(<<"http://www.google.com/q?r?">>)),
        ?_assertEqual(<<"http://www.google.com/q?r?s">>, canonc:canonicalize(<<"http://www.google.com/q?r?s">>)),
        ?_assertEqual(<<"http://evil.com/foo">>, canonc:canonicalize(<<"http://evil.com/foo#bar#baz">>)),
        ?_assertEqual(<<"http://evil.com/foo;">>, canonc:canonicalize(<<"http://evil.com/foo;">>)),
        ?_assertEqual(<<"http://evil.com/foo?bar;">>, canonc:canonicalize(<<"http://evil.com/foo?bar;">>)),
        ?_assertEqual(<<"http://%01%80.com/">>, canonc:canonicalize(<<"http://\x01\x80.com/">>)),
        ?_assertEqual(<<"http://notrailingslash.com/">>, canonc:canonicalize(<<"http://notrailingslash.com">>)),
        ?_assertEqual(<<"http://www.gotaport.com/">>, canonc:canonicalize(<<"http://www.gotaport.com:1234/">>)),
        ?_assertEqual(<<"http://www.google.com/">>, canonc:canonicalize(<<"  http://www.google.com/  ">>)),
        ?_assertEqual(<<"http://%20leadingspace.com/">>, canonc:canonicalize(<<"http:// leadingspace.com/">>)),
        ?_assertEqual(<<"http://%20leadingspace.com/">>, canonc:canonicalize(<<"http://%20leadingspace.com/">>)),
        ?_assertEqual(<<"http://%20leadingspace.com/">>, canonc:canonicalize(<<"%20leadingspace.com/">>)),
        ?_assertEqual(<<"https://www.securesite.com/">>, canonc:canonicalize(<<"https://www.securesite.com/">>)),
        ?_assertEqual(<<"http://host.com/ab%23cd">>, canonc:canonicalize(<<"http://host.com/ab%23cd">>)),
        ?_assertEqual(<<"http://host.com/twoslashes?more//slashes">>, canonc:canonicalize(<<"http://host.com//twoslashes?more//slashes">>))
    ].

