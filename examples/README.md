# Fetch Database

    curl -d @example_query.json \
        -H "content-type: application/json" \
        -XPOST 'https://safebrowsing.googleapis.com/v4/threatListUpdates:fetch?key=API_KEY'
